from dataclasses import dataclass
from typing import Tuple

import numpy as np

class Geometry:
    def __init__(self, space, nT):
        self.space = space
        self.nsites = np.prod(self.space,dtype=int)
        self.nT = nT

    def neighbors(self, i):
        """
        Iterates through the 2*DIM neighbors of the given lattice site.
        """
        sz = 1
        for L in (self.nT,)+self.space:
            x = i%(sz*L)
            y = i-x
            yield y+(x+sz)%(sz*L)
            yield y+(x-sz)%(sz*L)
            sz *= L

    def direction(self,i,j):
        """
        Returns an integer representing the direction of the link between two
        lattice sites. If the sites are not adjacent, an error is raised.
        """
        d = 0
        sz = 1
        for L in (self.nT,)+self.space:
            x = i%(sz*L)
            y = i-x
            if j == y+(x+sz)%(sz*L) or j == y+(x-sz)%(sz*L):
                return d
            sz *= L
            d += 1
        raise f"sites {i} and {j} are not adjacent"

    def slice(self, t):
        """
        Iterates through all lattice sites of the specified time slice.
        """
        for x in np.arange(np.prod(self.space,dtype=int)):
            yield t + x*self.nT

    def coords(self, i):
        # TODO
        assert False

    def coord(self, i, d):
        # TODO
        assert False

@dataclass
class LatticeS(Geometry):
    geom: Tuple[int]
    nbeta: int
    nt: int

    def __post_init__(self):
        super().__init__(self.geom, self.nbeta+2*self.nt)
        self.nbeta1 = self.nbeta//2
        self.nbeta2 = self.nbeta-self.nbeta1
        self.dim = 1+len(self.geom)
        self.V = (self.nbeta+2*self.nt)*np.prod(self.geom,dtype=int)
        self.dt = np.vectorize(self.slice_dt)(np.arange(2*self.nt+self.nbeta))
        self.dts = np.zeros(self.V,dtype=np.complex128)
        for t in range(self.nT):
            for x in self.slice(t):
                self.dts[x] = self.slice_dt(t)

    def spacing_time(self, t):
        if t < self.nt:
            return 1j
        elif t < self.nt+self.nbeta1:
            return 1 + 0j
        elif t < 2*self.nt+self.nbeta1:
            return -1j
        else:
            return 1 + 0j

    def slice_dt(self, i):
        """
        Returns the `dt` for the time-slice that has the given lattice site in it.
        """
        t = i%(self.nbeta+2*self.nt)
        tp = (t+self.nbeta+2*self.nt-1)%(self.nbeta+2*self.nt)
        return (self.spacing_time(t) + self.spacing_time(tp))/2

    def spacing(self,i,j):
        """
        Returns the spacing between two lattice sites. If `i` and `j` are not
        adjaceself.nt, an error is raised.
        """
        d = self.direction(i,j)
        if d > 0:
            return 1
        else:
            t = i%(self.nbeta+2*self.nt)
            tp = j%(self.nbeta+2*self.nt)
            if t == (tp+1)%(self.nbeta+2*self.nt):
                t = tp
            else:
                assert tp == (t+1)%(self.nbeta+2*self.nt)
            return self.spacing_time(t)

@dataclass
class LatticeL(Geometry):
    geom: Tuple[int]
    nbeta: int
    nt: int

    def __post_init__(self):
        super().__init__(self.geom, self.nbeta+2*self.nt)
        self.dim = 1+len(self.geom)
        self.V = (self.nbeta+2*self.nt)*np.prod(self.geom,dtype=int)
        self.dt = np.vectorize(self.slice_dt)(np.arange(2*self.nt+self.nbeta))
        self.dts = np.zeros(self.V,dtype=np.complex128)
        for t in range(self.nT):
            for x in self.slice(t):
                self.dts[x] = self.slice_dt(t)

    def spacing_time(self, t):
        if t < self.nt:
            return 1j
        elif t < 2*self.nt:
            return -1j
        else:
            return 1 + 0j

    def slice_dt(self, i):
        """
        Returns the `dt` for the time-slice that has the given lattice site in it.
        """
        t = i%(self.nbeta+2*self.nt)
        tp = (t+self.nbeta+2*self.nt-1)%(self.nbeta+2*self.nt)
        return (self.spacing_time(t) + self.spacing_time(tp))/2

    def spacing(self,i,j):
        """
        Returns the spacing between two lattice sites. If `i` and `j` are not
        adjaceself.nt, an error is raised.
        """
        d = self.direction(i,j)
        if d > 0:
            return 1
        else:
            t = i%(self.nbeta+2*self.nt)
            tp = j%(self.nbeta+2*self.nt)
            if t == (tp+1)%(self.nbeta+2*self.nt):
                t = tp
            else:
                assert tp == (t+1)%(self.nbeta+2*self.nt)
            return self.spacing_time(t)

Lattice = LatticeS

@dataclass
class Model:
    lattice: Lattice
    m2: float
    lamda: float

    def __post_init__(self):
        self.action_eff = np.vectorize(self.action_eff1, signature='(n)->()')

    def invprop(self, zeta):
        """
        Returns the inverse propagator for an auxiliary field of zeta. The action
        contains a term of the form (phi.T @ M @ phi / 2).
        """
        lattice = self.lattice
        assert zeta.shape == (lattice.V,)
        M = np.zeros((lattice.V,lattice.V), dtype=np.complex128)
        for i in range(lattice.V):
            M[i,i] = lattice.slice_dt(i) * (self.m2 + 1j*zeta[i])
        for i in range(lattice.V):
            for j in lattice.neighbors(i):
                if lattice.direction(i,j) == 0:
                    dt = lattice.spacing(i,j)
                else:
                    dt = lattice.slice_dt(i)
                a = lattice.spacing(i,j)
                if j > i:
                    M[i,j] = M[j,i] = -dt/a**2
                    M[i,i] += dt/a**2
                    M[j,j] += dt/a**2
        return M

    def prop(self, zeta):
        """
        Returns the propagator for an auxiliary field of zeta.
        """
        M = self.invprop(zeta)
        return np.linalg.inv(M)

    def logdetinvprop(self, zeta):
        """
        Returns log(det(M)) + C, where M is the inverse propagator returned by
        `invprop`, and C is an arbitrary (zeta-independent) constant.
        """
        # TODO this gets the phase wrong
        M = self.invprop(zeta)
        sgn, ld = np.linalg.slogdet(M)
        return ld + np.log(sgn)

    def action_eff1(self, zeta):
        """
        Returns the effective action (per flavor) for an auxiliary field of zeta.
        """
        return np.sum(self.lattice.dts*zeta**2)/(16*self.lamda) + 0.5 * self.logdetinvprop(zeta)

