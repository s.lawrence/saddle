#!/usr/bin/env python

"""
Checking the propagator.
"""

import numpy as np
import scalar


##################
### PARAMETERS ###

# Geometry of the spatial lattice
GEOM = ()

# Number of Euclidean, Minkowski time steps
nbeta, nt = 9, 9

# Lattice parameters
m2 = 1
#m2 = (2*np.pi / 30)**2


###############
### LATTICE ###

lattice = scalar.Lattice(geom=GEOM,nbeta=nbeta,nt=nt)
model = scalar.Model(lattice, m2, 0)


##############
### ACTION ###

def action(phi):
    S = 0j
    for i in range(lattice.V):
        S += lattice.slice_dt(i) * m2*phi[i]**2/2
        j = (i+1)%(lattice.nbeta+2*lattice.nt)
        if lattice.direction(i,j) == 0:
            dt = lattice.spacing(i,j)
        else:
            dt = lattice.slice_dt(i)
        a = lattice.spacing(i,j)
        S += 0.5 * dt * ((phi[i]-phi[j])/(a))**2
    return S

M = model.invprop(np.zeros(lattice.V))

rng = np.random.default_rng()
phi = rng.normal(size=lattice.V)
print(action(phi), phi.T @ M @ phi / 2)

print(np.diag(np.linalg.inv(M)))
print(np.linalg.det(M))
