#!/usr/bin/env python

import numpy as np
import sys

nbeta = int(sys.argv[1])
nt = int(sys.argv[2])
m2 = float(sys.argv[3])
g = float(sys.argv[4])
K = int(sys.argv[5])
N = 1
D = K**N

# Create the basic operators.
a_ = np.zeros((K,K))
for i in range(1,K):
    a_[i-1,i] = np.sqrt(i)

a = [1]*N
for n in range(N):
    for n_ in range(N):
        if n == n_:
            a[n] = np.kron(a[n], a_)
        else:
            a[n] = np.kron(a[n], np.eye(K))

adag = [1]*N
for n in range(N):
    adag[n] = a[n].T.conj()

x = [1]*N
p = [1]*N
for n in range(N):
    x[n] = (a[n] + adag[n]) / np.sqrt(2)
    p[n] = 1j*(a[n] - adag[n]) / np.sqrt(2)

# Composite field-squared operator.
x2 = np.zeros((D,D))
for n in range(N):
    x2 += x[n]@x[n]


def loss(izeta):
    # Construct the Hamiltonian.
    Hx = np.zeros((D,D), dtype=np.complex128)
    Hp = np.zeros((D,D), dtype=np.complex128)
    for n in range(N):
        Hx += (m2+izeta)/2 * x[n]@x[n]
        Hp += p[n]@p[n]/2

    H = Hx + Hp

    # Trotterized time evolution operators. There are two; one in real time and one
    # in imaginary time.

    # Diagonalize Hx and Hp
    xvals, xvecs = np.linalg.eigh(Hx)
    pvals, pvecs = np.linalg.eigh(Hp)

    # Exponentiate each, twice.
    Uxtemphalf = xvecs @ np.diag(np.exp(-xvals/2)) @ xvecs.T.conj()
    Uxtimehalf = xvecs @ np.diag(np.exp(-1j * xvals/2)) @ xvecs.T.conj()
    Uptemp = pvecs @ np.diag(np.exp(-pvals)) @ pvecs.T.conj()
    Uptime = pvecs @ np.diag(np.exp(-1j * pvals)) @ pvecs.T.conj()

    # Put things together.
    Utemp1 = Uxtemphalf @ Uptemp @ Uxtemphalf
    Utime1 = Uxtimehalf @ Uptime @ Uxtimehalf
    Udagtime1 = Utime1.T.conj()

    Utemp = np.eye(D)
    Utime = np.eye(D)
    for _ in range(nbeta):
        Utemp = Utemp1 @ Utemp
    for _ in range(nt):
        Utime = Utime1 @ Utime
    Udagtime = Utime.T.conj()

    rho = Utemp
    rho /= np.trace(rho)
    return ((izeta/(8*g) - 0.5 * np.trace(Udagtime @ x2 @ Utime @ rho)/N)**2).real
    #print(izeta, izeta/(8*g) - 0.5 * np.trace(x2 @ rho)/N, 0.5*np.trace(x2 @ rho)/N)

from scipy.optimize import minimize

print(minimize(loss, 0.1))
