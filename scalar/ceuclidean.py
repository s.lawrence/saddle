#!/usr/bin/env python

"""
Lattice scalar field theory, in the large-N limit. Here expectation values are
evaluated by a Monte Carlo.
"""

import ast
from functools import partial
import sys

import numpy as np
from scipy.optimize import minimize

import scalar


##################
### PARAMETERS ###

# Geometry of the spatial lattice
GEOM = ast.literal_eval(sys.argv[1])

# Number of Euclidean, Minkowski time steps
nbeta = int(sys.argv[2])
nt = 0

# Lattice parameters
m2 = float(sys.argv[3])
lamda = float(sys.argv[4])

N = int(sys.argv[5])


###############
### LATTICE ###

lattice = scalar.Lattice(geom=GEOM,nbeta=nbeta,nt=nt)
model = scalar.Model(lattice, m2, lamda)
V = lattice.V


##############
### SADDLE ###

def gradient(zeta):
    """
    Returns the first derivatives of the effective action with respect to zeta.
    """
    M = model.invprop(zeta)
    Minv = np.linalg.inv(M)
    d1S = np.zeros(lattice.V,dtype=np.complex128)
    for t in range(lattice.nT):
        for a in lattice.slice(t):
            d1S[a] += lattice.dt[t] * zeta[a] / (8*lamda)
            d1S[a] += lattice.dt[t] * 0.5j * Minv[a,a]
    return d1S

# Search for a constant saddle point.
def minf(iz):
    return np.linalg.norm(gradient(1j*iz*np.ones(lattice.V)))
result = minimize(minf, 0.1)
zeta0 = 1j*result.x*np.ones(lattice.V)


M = model.invprop(zeta0)
Minv = np.linalg.inv(M)


# Derivatives about the saddle point.

# The first derivatives should vanish. Before doing anything else, let's check that.
d1S = np.zeros(lattice.V,dtype=np.complex128)
for t in range(lattice.nT):
    for a in lattice.slice(t):
        d1S[a] += lattice.dt[t] * zeta0[a] / (8*lamda)
        d1S[a] += lattice.dt[t] * 0.5j * Minv[a,a]
if np.sum(np.abs(d1S)) > 1e-5:
    print('Not a saddle point! dS follows')
    print(d1S)
    sys.exit(1)

# The second derivatives (Hessian)
d2S = np.zeros((lattice.V,lattice.V),dtype=np.complex128)
for t in range(lattice.nT):
    for a in lattice.slice(t):
        d2S[a,a] = lattice.dt[t] / (8 * lamda)
for t in range(lattice.nT):
    for tp in range(lattice.nT):
        for x in lattice.slice(t):
            for xp in lattice.slice(tp):
                Del = lattice.dt[t] * lattice.dt[tp]
                d2S[x,xp] += 0.5 * Del * Minv[x,xp] * Minv[xp,x]

# Diagonalize d2S
d2Svals, d2Svecs = np.linalg.eig(d2S)
inv_d2Svecs = np.linalg.inv(d2Svecs)
# d2S is now given by: d2Svecs @ np.diag(d2Svals) @ inv_d2Svecs

# Construct scaling matrix for sampling from the gaussian.
inv_sqrt_d2S = d2Svecs @ np.diag(d2Svals**-0.5) @ inv_d2Svecs


###################
### MONTE CARLO ###

NBATCH = 1 << 5
NSAMPLE = 1 << 0
rng = np.random.default_rng()

nums = [[] for t in range(nt+1)]
dens = []

try:
    while True:
        for batch in range(NBATCH):
            sigma = rng.normal(size=(NSAMPLE,V))
            # Sampling action
            Sp = np.sum(sigma**2,axis=-1)/2 + N*model.action_eff1(zeta0)
            sigma = np.einsum('ab,cb->ca', inv_sqrt_d2S, sigma)

            zeta = zeta0 + sigma / np.sqrt(N)

            # True action
            S = N * model.action_eff(zeta)

            G = np.vectorize(model.prop,signature='(n)->(n,n)')(zeta)

            den = np.exp(-S+Sp)
            dens.append(np.mean(den))

            for t in range(nt+1):
                num = np.exp(-S+Sp) * G[:,t,0]
                nums[t].append(np.mean(num))

        print(f'{np.mean(dens)} {np.mean(dens/np.abs(dens))}')

except KeyboardInterrupt:
    print('')
    print(f'Reweighting: {np.mean(dens)}')
    print(f'Sign:        {np.mean(dens/np.abs(dens))}')
    print('== EXPECTATIONS ==')
    for t in range(nt+1):
        ex = sum(nums[t]) / sum(dens)
        print(f'{t} {ex.real} {ex.imag}')

