#!/usr/bin/env python

"""
Lattice scalar field theory, in the large-N limit. Here expectation values are
evaluated by a Monte Carlo.
"""

import ast
import sys
import numpy as np
from scipy.optimize import minimize

import scalar


##################
### PARAMETERS ###

# Geometry of the spatial lattice
GEOM = ast.literal_eval(sys.argv[1])

# Number of Euclidean, Minkowski time steps
nbeta, nt = int(sys.argv[2]), int(sys.argv[3])

# Lattice parameters
m2 = float(sys.argv[4])
lamda = float(sys.argv[5])


###############
### LATTICE ###

lattice = scalar.Lattice(geom=GEOM,nbeta=nbeta,nt=nt)
model = scalar.Model(lattice, m2, lamda)
V = lattice.V


##############
### SADDLE ###

def gradient(zeta):
    """
    Returns the first derivatives of the effective action with respect to zeta.
    """
    M = model.invprop(zeta)
    Minv = np.linalg.inv(M)
    d1S = np.zeros(lattice.V,dtype=np.complex128)
    for a in range(lattice.V):
        d1S[a] += lattice.dt[a] * zeta[a] / (8*lamda)
        d1S[a] += lattice.dt[a] * 0.5j * Minv[a,a]
    return d1S

if False:
    # Search for a pure-imaginary saddle point
    result = minimize(lambda x: np.linalg.norm(gradient(-1j*x)), np.zeros(lattice.V))
    print(result.success, result.message)
    zeta = -1j*result.x
    print(1j*zeta)

if True:
    # Search for a symmetric saddle point
    def make_zeta(x):
        # Put real and imaginary parts together.
        z = x[:lattice.nT//2] + 1j*x[lattice.nT//2:]
        z = np.concatenate([z, -1j*(1j*z).conj()])
        zeta = np.zeros(lattice.V,dtype=np.complex128)
        for t in range(lattice.nT):
            for a in lattice.slice(t):
                zeta[a] = z[t]
        return zeta
    minf = lambda x: np.linalg.norm(gradient(make_zeta(x)))
    result = minimize(minf, np.zeros(lattice.nT))
    #print(result.success, result.message)
    zeta = make_zeta(result.x)
    #print(zeta)


M = model.invprop(zeta)
Minv = np.linalg.inv(M)

# Derivatives about the saddle point.

# The first derivatives should vanish. Before doing anything else, let's check that.
d1S = lattice.dt*zeta / (8*lamda)
for a in range(V):
    d1S[a] += lattice.dt[a] * 0.5j * Minv[a,a]
if np.sum(np.abs(d1S)) > 1e-5:
    print('Not a saddle point! dS follows')
    print(d1S)
    sys.exit(1)

# The second derivatives (Hessian)
d2S = np.diag(lattice.dt) / (8 * lamda)
for a in range(V):
    for b in range(V):
        Del = lattice.dt[a] * lattice.dt[b]
        d2S[a,b] += 0.5 * Del * Minv[a,b] * Minv[b,a]

# The third derivatives
d3S = np.zeros((V,V,V), dtype=np.complex128)
for a in range(V):
    for b in range(V):
        for c in range(V):
            Del = lattice.dt[a] * lattice.dt[b] * lattice.dt[c]
            d3S[a,b,c] += -0.5j * Del * Minv[a,c]*Minv[c,b]*Minv[b,a]
            d3S[a,b,c] += -0.5j * Del * Minv[a,b]*Minv[b,c]*Minv[c,a]

# The fourth derivatives
d4S = np.zeros((V,V,V,V), dtype=np.complex128)
for a in range(V):
    for b in range(V):
        for c in range(V):
            for d in range(V):
                Del = lattice.dt[a] * lattice.dt[b] * lattice.dt[c] * lattice.dt[d]
                d4S[a,b,c,d] += -0.5 * Del * Minv[a,d]*Minv[d,c]*Minv[c,b]*Minv[b,a]
                d4S[a,b,c,d] += -0.5 * Del * Minv[a,c]*Minv[c,d]*Minv[d,b]*Minv[b,a]
                d4S[a,b,c,d] += -0.5 * Del * Minv[a,c]*Minv[c,b]*Minv[b,d]*Minv[d,a]
                d4S[a,b,c,d] += -0.5 * Del * Minv[a,d]*Minv[d,b]*Minv[b,c]*Minv[c,a]
                d4S[a,b,c,d] += -0.5 * Del * Minv[a,b]*Minv[b,d]*Minv[d,c]*Minv[c,a]
                d4S[a,b,c,d] += -0.5 * Del * Minv[a,b]*Minv[b,c]*Minv[c,d]*Minv[d,a]

# Diagonalize A
d2Svals, d2Svecs = np.linalg.eig(d2S)
inv_d2Svecs = np.linalg.inv(d2Svecs)
# d2S is now given by: d2Svecs @ np.diag(d2Svals) @ inv_d2Svecs

# Construct scaling matrix for sampling from the gaussian.
inv_sqrt_d2S = d2Svecs @ np.diag(d2Svals**-0.5) @ inv_d2Svecs


###################
### MONTE CARLO ###

NBATCH = 1 << 8
NSAMPLE = 1 << 5
rng = np.random.default_rng()

Enums0 = [[] for t in range(lattice.V)]
Edens0 = []
Enums1 = [[] for t in range(lattice.V)]
Edens1 = []

for batch in range(NBATCH):
    sigma = rng.normal(size=(NSAMPLE,V))
    sigma = np.einsum('ab,cb->ca', inv_sqrt_d2S, sigma)

    Eden0 = np.ones(NSAMPLE)
    Edens0.append(np.sum(Eden0))

    sum3 = np.einsum('ia,ib,ic,abc->i', sigma, sigma, sigma, d3S)
    sum4 = np.einsum('ia,ib,ic,id,abcd->i', sigma, sigma, sigma, sigma, d4S)

    Eden1 = 0
    Eden1 += -1/24 * sum4
    Eden1 += (1/6)**2 * sum3**2

    dt_sigma = lattice.dt*sigma
    prod1 = np.einsum('ab,ib,bc->iac', Minv, dt_sigma, Minv)
    prod2 = np.einsum('ab,ib,bc,ic,cd->iad', Minv, dt_sigma, Minv, dt_sigma, Minv)

    Edens1.append(np.sum(Eden1))

    for x in range(lattice.V):
        Enum0 = np.zeros(NSAMPLE) + Minv[x,0]

        Enum1 = 0
        Enum1 += -1/24 * sum4 * Minv[x,0]
        Enum1 += (1/6)**2 * sum3**2 * Minv[x,0]
        Enum1 += -1j * prod1[:,x,0] * (-1/6) * sum3
        Enum1 += -prod2[:,x,0]

        Enums0[x].append(np.sum(Enum0))
        Enums1[x].append(np.sum(Enum1))


expect0 = [0 for i in range(lattice.V)]
expect1 = [0 for i in range(lattice.V)]

for x in range(lattice.V):
    expect0[x] = sum(Enums0[x])/sum(Edens0)
    expect1[x] = sum(Enums1[x])/sum(Edens0)
    expect1[x] += -(sum(Enums0[x])/sum(Edens0))*(sum(Edens1)/sum(Edens0))

if False:
    # Print the whole grid.
    for t in range(nt+1):
        for x in lattice.slice(t):
            print(f'{t} {x} {expect0[x]} {expect1[x]}')

if True:
    # Print just the zeroth component.
    for t in range(nt+1):
        tot0 = 0.j
        tot1 = 0.j
        for x in lattice.slice(t):
            tot0 += expect0[x]
            tot1 += expect1[x]
        print(f'{t} {tot0} {tot1}')


if False:
    # The old printing, really only good for 0+1
    if nt > 0:
        for t in range(nt+1):
            expect0 = sum(Enums0[t])/sum(Edens0)
            expect1 = sum(Enums1[t])/sum(Edens0)
            expect1 += -(sum(Enums0[t])/sum(Edens0))*(sum(Edens1)/sum(Edens0))
            print(f'{t}  {expect0} {expect1}')
    else:
        for t in range(nbeta):
            expect0 = sum(Enums0[t])/sum(Edens0)
            expect1 = sum(Enums1[t])/sum(Edens0)
            expect1 += -(sum(Enums0[t])/sum(Edens0))*(sum(Edens1)/sum(Edens0))
            print(f'{t}  {expect0} {expect1}')

