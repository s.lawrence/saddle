#!/usr/bin/env python

# Evaluating expectation values by ordinary Monte Carlo, at finite N.

import sys
import numpy as np

N = int(sys.argv[1])
m2 = float(sys.argv[2])
lamda = float(sys.argv[3])
beta = int(sys.argv[4])
meas = int(sys.argv[5])

skip = 5*N*beta

def action(phi):
    S = 0.
    S += m2 / 2. * np.sum(phi**2)
    S += np.sum((phi - np.roll(phi,axis=0,shift=1))**2) / 2.
    S += lamda/N * np.sum(np.sum(phi**2,axis=1)**2)
    return S

phi = np.zeros((beta,N))
act = action(phi)
for _ in range(meas):
    for _ in range(skip):
        phip = phi + np.random.normal(size=(beta,N))/np.sqrt(beta*N)
        actp = action(phip)
        if np.random.uniform() < np.exp(act-actp):
            phi = phip
            act = actp
    phi2 = np.sum(phi**2,axis=1)
    print(' '.join(list(str(x) for x in phi2)))

