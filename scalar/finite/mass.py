#!/usr/bin/env python

import sys
import numpy as np

fn = sys.argv[1]

with open(fn) as f:
    dat = np.array([[float(x) for x in l.split()] for l in f.readlines()])

beta = dat.shape[1]

import matplotlib.pyplot as plt

cors = np.zeros(dat.shape)
for i in range(beta):
    for j in range(beta):
        cors[:,(j-i)%beta] += dat[:,i]*dat[:,j]



plt.errorbar(range(beta), np.mean(cors,axis=0), yerr=np.std(cors,axis=0)/np.sqrt(dat.shape[0]))
plt.show()

