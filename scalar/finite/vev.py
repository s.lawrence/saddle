#!/usr/bin/env python

# Evaluating expectation values by ordinary Monte Carlo, at finite N.

import sys
import jax
#import jax.numpy as jnp
import numpy as np
jnp = np

N = int(sys.argv[1])
m2 = float(sys.argv[2])
lamda = float(sys.argv[3])
beta = int(sys.argv[4])
meas = int(sys.argv[5])

skip = 5*N*beta

#@jax.jit
def action(phi):
    S = 0.
    S += m2 / 2. * jnp.sum(phi**2)
    S += jnp.sum((phi - jnp.roll(phi,axis=0,shift=1))**2) / 2.
    S += lamda/N * jnp.sum(jnp.sum(phi**2,axis=1)**2)
    return S

phi = jnp.zeros((beta,N))
obs = []
act = action(phi)
for _ in range(meas):
    for _ in range(skip):
        phip = phi + np.random.normal(size=(beta,N))/jnp.sqrt(beta*N)
        actp = action(phip)
        if np.random.uniform() < jnp.exp(act-actp):
            phi = phip
            act = actp
    obs.append(jnp.sum(phi**2)/beta/N)
    print(np.mean(obs), np.std(obs)/jnp.sqrt(len(obs)))

