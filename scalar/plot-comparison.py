#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np

with open('finite-0.2-0.1-6.dat') as f:
    lines = [l for l in f.readlines() if len(l.split()) > 1]
    finite = np.array([[float(x) for x in l.split()] for l in lines])

N = finite[:,0]
expect = finite[:,1]
error = finite[:,2]

plt.figure(figsize=(5,3.5), dpi=300)
#plt.errorbar(1/N, expect, yerr=error, fmt=',', label='Monte Carlo')
plt.scatter(1/N, expect, label='Monte Carlo', color='black')
x = np.linspace(0,1,100)
plt.plot(x, 0.7-0.3*x, label='Expansion')
plt.legend(loc='best')
plt.xlabel('$1/N$')
plt.ylabel('$\\langle \\phi^2\\rangle$')
plt.tight_layout()
#plt.show()
plt.savefig('verification.png', transparent=True)

