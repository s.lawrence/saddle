#!/usr/bin/env python

"""
Free lattice scalar field theory, as a test of the Schwinger-Keldysh contour.
"""

import sys
import numpy as np
from scipy.optimize import minimize

import scalar

##################
### PARAMETERS ###

# Geometry of the spatial lattice
GEOM = (3,)

# Number of Euclidean, Minkowski time steps
nbeta, nt = 6, 20

# Lattice parameters
#m2 = (2*np.pi / 30)**2
m2 = 0.1

###############
### LATTICE ###

lattice = scalar.LatticeS(geom=GEOM,nbeta=nbeta,nt=nt)
model = scalar.Model(lattice, m2, 0)



M = model.invprop(np.zeros(lattice.V))
Minv = np.linalg.inv(M)

# Diagonalize M
Mvals, Mvecs = np.linalg.eig(M)
inv_Mvecs = np.linalg.inv(Mvecs)
# M is now given by: Mvecs @ np.diag(Mvals) @ inv_Mvecs

# Construct scaling matrix for sampling from the gaussian.
inv_sqrt_M = Mvecs @ np.diag(Mvals**-0.5) @ inv_Mvecs


###################
### MONTE CARLO ###

NBATCH = 1 << 4
NSAMPLE = 1 << 6
rng = np.random.default_rng()

vevs = [[] for t in range(lattice.nt+1)]
cors = [[] for t in range(lattice.nt+1)]
for batch in range(NBATCH):
    phi = rng.normal(size=(NSAMPLE,lattice.V))
    phi = np.einsum('ab,cb->ca', inv_sqrt_M, phi)

    for t in range(lattice.nt+1):
        vev = 0.j
        for x in lattice.slice(t):
            vev += np.mean(phi[:,x]*phi[:,x],axis=0)
        vevs[t].append(vev/lattice.nsites)

        cor = 0.j
        for x in lattice.slice(0):
            for y in lattice.slice(t):
                cor += np.mean(phi[:,y]*phi[:,x]/lattice.nsites,axis=0)
        cors[t].append(cor)

vevs = np.mean(np.array(vevs),axis=1)
cors = np.mean(np.array(cors),axis=1)

for t in range(nt+1):
    print(t, vevs[t].real, cors[t].real, cors[t].imag)

