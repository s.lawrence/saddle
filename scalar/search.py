#!/usr/bin/env python

"""
Searching for saddle points
"""

import sys
import numpy as np
from scipy import optimize
from scipy.optimize import minimize

import scalar

##################
### PARAMETERS ###

# Geometry of the spatial lattice
GEOM = ()

# Number of Euclidean, Minkowski time steps
nbeta, nt = 6, 8

# Lattice parameters
m2 = 0.1
lamda = 0.01


###############
### LATTICE ###

lattice = scalar.LatticeS(geom=GEOM,nbeta=nbeta,nt=nt)
V = lattice.V
SK = np.vectorize(lattice.slice_dt)(np.arange(2*nt+nbeta))

model = scalar.Model(lattice, m2, lamda)


##############
### SADDLE ###

def gradient(zeta):
    """
    Returns the first derivatives of the effective action with respect to zeta.
    """
    M = model.invprop(zeta)
    Minv = np.linalg.inv(M)
    d1S = np.zeros(V,dtype=np.complex128)
    for a in range(V):
        d1S[a] += SK[a] * zeta[a] / (8*lamda)
        d1S[a] += SK[a] * 0.5j * Minv[a,a]
    return d1S

if False:
    zeta = np.random.normal(size=V) + 1j*np.random.normal(size=V)
    d1S = gradient(zeta)
    h = 1e-4
    for i in range(V):
        zetap = zeta.copy()
        zetap[i] += h
        print((action1(zetap)-action1(zeta))/h, d1S[i])
    sys.exit(0)

# Finding the saddle point

if False:
    # Search for a constant saddle point.
    result = minimize(lambda x: -action(-1j*x*np.ones(V)).real, 0, bounds=[(0,None)])
    print(result.success, result.message)
    print(result.x)
    zeta = -1j*result.x * np.ones(V)

if True:
    # Search for an arbitrary saddle point
    result = minimize(lambda x: np.linalg.norm(gradient(x[:V]+1j*x[V:])), np.zeros(2*V))
    print(result.success, result.message)
    zeta = result.x[:V] + 1j*result.x[V:]
    print(zeta)

if False:
    # Search for a pure-imaginary saddle point
    result = minimize(lambda x: np.linalg.norm(gradient(-1j*x)), np.zeros(V))
    print(result.success, result.message)
    zeta = -1j*result.x
    print(1j*zeta)

if False:
    # Maximize in pure-imaginary space
    result = minimize(lambda x: -action(-1j*x).real, np.zeros(V))
    print(result.success, result.message)
    print(result.x)
    zeta = -1j*result.x
    print(1j*zeta)

if True:
    # Search for a symmetric saddle point
    def expand(zeta1):
        return np.concatenate([zeta1, -1j*(1j*zeta1).conj()])
    def fromx(x):
        return expand(x[:V//2]+1j*x[V//2:])
    result = minimize(lambda x: np.linalg.norm(gradient(fromx(x))), np.zeros(V))
    print(result.success, result.message)
    zeta = fromx(result.x)
    print(zeta)

if False:
    # Experimental
    bounds = [(-1,1)]*V
    result = optimize.shgo(lambda x: np.linalg.norm(gradient(1j*x)), bounds)
    print(result.success, result.message)
    print(result.x)
    zeta = 1j*result.x


d1S = gradient(zeta)
if np.max(np.abs(d1S)) > 1e-6:
    print('Not a saddle point!')
    print(d1S)

if False:
    M = model.invprop(0*zeta)
    Minv = np.linalg.inv(M)
    print(0.5*Minv[0,0], 1j*zeta/(8*lamda))
