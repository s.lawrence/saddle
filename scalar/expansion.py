#!/usr/bin/env python

"""
Lattice scalar field theory, in the large-N limit.
"""

import numpy as np
from scipy.optimize import minimize


##################
### PARAMETERS ###

# Geometry of the spatial lattice
GEOM = ()

# Number of Euclidean, Minkowski time steps
nbeta, nt = 4, 0

# Lattice parameters
m2 = 0.2
lamda = 0.1


###############
### LATTICE ###

# Lattice dimension
DIM = 1+len(GEOM)

# Lattice volume
V = int((nbeta+2*nt)*np.prod(GEOM))

def neighbors(i):
    """
    Iterates through the 2*DIM neighbors of the given lattice site.
    """
    sz = 1
    for L in GEOM+(nbeta+2*nt,):
        x = i%(sz*L)
        y = i-x
        yield y+(x+sz)%(sz*L)
        yield y+(x-sz)%(sz*L)
        sz *= L


##############
### ACTION ###

def invprop(zeta):
    """
    Returns the inverse propagator for a constant auxiliary field of zeta. The
    action contains a term of the form (phi.T @ M @ phi / 2).
    """
    M = np.eye(V)*(m2 + 1j*zeta)
    for i in range(V):
        for j in neighbors(i):
            if j > i:
                M[i,j] = M[j,i] = -1
                M[i,i] += 1.
                M[j,j] += 1.
    return M

def prop(zeta):
    """
    Returns the propagator for a constant auxiliary field of zeta.
    """
    M = invprop(zeta)
    return np.linalg.inv(M)

def logdetinvprop(zeta):
    """
    Returns log(det(M)) + C, where M is the inverse propagator returned by
    `invprop`, and C is an arbitrary (zeta-independent) constant.
    """
    M = invprop(zeta)
    sgn, ld = np.linalg.slogdet(M)
    return ld + np.log(sgn)

@np.vectorize
def action(zeta):
    """
    Returns the effective action (per flavor) for a constant auxiliary field of
    zeta.
    """
    return V*zeta**2/(16*lamda) + 0.5 * logdetinvprop(zeta)


# Finding the saddle point
zeta0 = -1j * minimize(lambda z: -action(-1j*z).real, 0.2, bounds=[(0,None)]).x
M = invprop(zeta0)
Minv = np.linalg.inv(M)

if True:
    import matplotlib.pyplot as plt
    zeta = np.linspace(-2,2,10000)
    plt.plot(zeta, action(-1j*zeta).real)
    plt.show()

# The second derivatives (Hessian)
d2S = np.eye(V, dtype=np.complex128) / (8 * lamda)
for a in range(V):
    for b in range(V):
        d2S[a,b] += 0.5 * Minv[a,b] * Minv[b,a]

# The third derivatives
d3S = np.zeros((V,V,V), dtype=np.complex128)
for a in range(V):
    for b in range(V):
        for c in range(V):
            d3S[a,b,c] += -0.5j * Minv[a,c]*Minv[c,b]*Minv[b,a]
            d3S[a,b,c] += -0.5j * Minv[a,b]*Minv[b,c]*Minv[c,a]

# The fourth derivatives
d4S = np.zeros((V,V,V,V), dtype=np.complex128)
for a in range(V):
    for b in range(V):
        for c in range(V):
            for d in range(V):
                d4S[a,b,c,d] += -0.5 * Minv[a,d]*Minv[d,c]*Minv[c,b]*Minv[b,a]
                d4S[a,b,c,d] += -0.5 * Minv[a,c]*Minv[c,d]*Minv[d,b]*Minv[b,a]
                d4S[a,b,c,d] += -0.5 * Minv[a,c]*Minv[c,b]*Minv[b,d]*Minv[d,a]
                d4S[a,b,c,d] += -0.5 * Minv[a,d]*Minv[d,b]*Minv[b,c]*Minv[c,a]
                d4S[a,b,c,d] += -0.5 * Minv[a,b]*Minv[b,d]*Minv[d,c]*Minv[c,a]
                d4S[a,b,c,d] += -0.5 * Minv[a,b]*Minv[b,c]*Minv[c,d]*Minv[d,a]

# Partition function

# The zero point of the action, a factor of N^(V/2), and the Hessian of the
# saddle point have all been factored out.
Z = 1.

d1Z = 0.

# Numerator of expectation value
#N = 1j*zeta0/(4*lamda)
E = Minv[0,0]

d1E = 0.

print(E/Z, d1E/Z - E/Z * d1Z/Z)

