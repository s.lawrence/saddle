#!/usr/bin/env python

"""
Expectation values in an arbitrary background
"""

import sys
import numpy as np
from scipy import optimize
from scipy.optimize import minimize

import scalar


##################
### PARAMETERS ###

# Geometry of the spatial lattice
GEOM = (4,)

# Number of Euclidean, Minkowski time steps
nbeta, nt = 6, 20

# Lattice parameters
#m2 = (2*np.pi / 30)**2
m2 = 0.1
izeta = 0


###############
### LATTICE ###

lattice = scalar.Lattice(geom=GEOM,nbeta=nbeta,nt=nt)
model = scalar.Model(lattice, m2, 0)


##########################
### EXPECTATION VALUES ###

zeta = -1j*izeta * np.ones(lattice.V)

M = model.invprop(zeta)
Minv = np.linalg.inv(M)
for t in range(lattice.nt):
    tot = 0
    for x in lattice.slice(0):
        for xp in lattice.slice(t):
            tot += Minv[xp,x].real
    print(t, tot/np.prod(GEOM))
