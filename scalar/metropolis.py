#!/usr/bin/env python

"""
Lattice scalar field theory, in the large-N limit. Here expectation values are
evaluated by MCMC.
"""

import ast
from functools import partial
import sys

import numpy as np
from scipy.optimize import minimize

import scalar


##################
### PARAMETERS ###

# Geometry of the spatial lattice
GEOM = ast.literal_eval(sys.argv[1])

# Number of Euclidean, Minkowski time steps
nbeta, nt = int(sys.argv[2]), int(sys.argv[3])

# Lattice parameters
m2 = float(sys.argv[4])
lamda = float(sys.argv[5])

N = int(sys.argv[6])


###############
### LATTICE ###

lattice = scalar.Lattice(geom=GEOM,nbeta=nbeta,nt=nt)
model = scalar.Model(lattice, m2, lamda)
V = lattice.V


##############
### SADDLE ###

def gradient(zeta):
    """
    Returns the first derivatives of the effective action with respect to zeta.
    """
    M = model.invprop(zeta)
    Minv = np.linalg.inv(M)
    d1S = np.zeros(lattice.V,dtype=np.complex128)
    for a in range(lattice.V):
        d1S[a] += lattice.dt[a] * zeta[a] / (8*lamda)
        d1S[a] += lattice.dt[a] * 0.5j * Minv[a,a]
    return d1S

if True:
    # Search for a symmetric saddle point
    def make_zeta(x):
        # Put real and imaginary parts together.
        z = x[:lattice.nT//2] + 1j*x[lattice.nT//2:]
        z = np.concatenate([z, -1j*(1j*z).conj()])
        zeta = np.zeros(lattice.V,dtype=np.complex128)
        for t in range(lattice.nT):
            for a in lattice.slice(t):
                zeta[a] = z[t]
        return zeta
    minf = lambda x: np.linalg.norm(gradient(make_zeta(x)))
    result = minimize(minf, np.zeros(lattice.nT))
    print(result.success, result.message)
    zeta0 = make_zeta(result.x)
    print(zeta0)


M = model.invprop(zeta0)
Minv = np.linalg.inv(M)


# Derivatives about the saddle point.

# The first derivatives should vanish. Before doing anything else, let's check that.
d1S = lattice.dt*zeta0 / (8*lamda)
for a in range(V):
    d1S[a] += lattice.dt[a] * 0.5j * Minv[a,a]
if np.sum(np.abs(d1S)) > 1e-5:
    print('Not a saddle point! dS follows')
    print(d1S)
    sys.exit(1)

# The second derivatives (Hessian)
d2S = np.diag(lattice.dt) / (8 * lamda)
for a in range(V):
    for b in range(V):
        Del = lattice.dt[a] * lattice.dt[b]
        d2S[a,b] += 0.5 * Del * Minv[a,b] * Minv[b,a]

# Diagonalize d2S
d2Svals, d2Svecs = np.linalg.eig(d2S)
inv_d2Svecs = np.linalg.inv(d2Svecs)
# d2S is now given by: d2Svecs @ np.diag(d2Svals) @ inv_d2Svecs

# Construct scaling matrix for sampling from the gaussian.
inv_sqrt_d2S = d2Svecs @ np.diag(d2Svals**-0.5) @ inv_d2Svecs


###################
### MONTE CARLO ###

NSKIP = 10
NMEAS = 100

rng = np.random.default_rng()

nums = [[] for t in range(nt+1)]
dens = []

sigma = np.zeros(V)

try:
    while True:
        accepted = 0
        for meas in range(NMEAS):
            for skip in range(NSKIP):
                dsigma = rng.normal(size=V)/np.sqrt(V)
                sigmap = sigma + dsigma
                zeta = zeta0 + np.einsum('ab,b->a', inv_sqrt_d2S, sigma) / np.sqrt(N)
                zetap = zeta0 + np.einsum('ab,b->a', inv_sqrt_d2S, sigmap) / np.sqrt(N)
                S = N * model.action_eff1(zeta)
                Sp = N * model.action_eff1(zetap)
                if rng.random() < np.exp((S-Sp).real):
                    sigma = sigmap
                    accepted += 1

            den = np.exp(-1j*S.imag)
            dens.append(den)

            G = model.prop(zeta)
            for t in range(nt+1):
                num = np.exp(-1j*S.imag) * G[t,0]
                nums[t].append(num)

        print(f'{accepted/(NMEAS*NSKIP)} {np.mean(dens)}')

except KeyboardInterrupt:
    print('')
    print(f'Sign: {np.mean(dens)}')
    print('== EXPECTATIONS ==')
    for t in range(nt+1):
        ex = sum(nums[t]) / sum(dens)
        print(f'{t} {ex.real} {ex.imag}')

