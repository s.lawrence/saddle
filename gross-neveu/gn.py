from dataclasses import dataclass
import itertools
from typing import Tuple

import jax
import jax.numpy as jnp
import numpy as np

def sk_contour(nbeta, nt, shape='L'):
    if shape == 'L':
        return np.concatenate([1j*np.ones(nt),-1j*np.ones(nt),np.ones(nbeta)])
    elif shape == 'S':
        nbeta1 = int(nbeta/2)
        nbeta2 = nbeta-nbeta1
        return np.concatenate([
            1j*np.ones(nt),
            np.ones(nbeta1),
            -1j*np.ones(nt),
            np.ones(nbeta2),
            ])
    else:
        raise "that contour is not implemented"

@dataclass
class Lattice:
    geom: Tuple[int]
    contour: np.array
    def __post_init__(self):
        self.D = len(self.geom)
        self.NT = len(self.contour)
        self.shape = (self.NT,)+self.geom
        self.Nsites = np.prod(self.shape, dtype=int)
        self.contour_site = np.array(
                [(self.contour[i]+self.contour[(i-1)%self.NT])/2 for i in range(self.NT)])
        self.dt_link = np.tile(self.contour.reshape((self.NT,)+(1,)*self.D), (1,)+self.geom)
        self.dt_site = np.tile(self.contour_site.reshape((self.NT,)+(1,)*self.D), (1,)+self.geom)

        # Indices
        self._index = {}
        self._coord = {}
        for (i, c) in enumerate(self.sites()):
            self._index[c] = i
            self._coord[i] = c

    def sites(self):
        yield from itertools.product(range(self.NT),*[range(x) for x in self.geom])

    def index(self, site):
        return self._index[site]

    def coordinates(self, idx):
        return self._coord[idx]

    def step(self, site, dim, dist):
        return tuple((site[d]+dist)%self.shape[d] if d == dim else site[d] for d in range(self.D+1))

    def neighbors(self, site):
        for d in range(self.D+1):
            yield self.step(site, d, -1)
            yield self.step(site, d, +1)


@dataclass
class Model:
    lattice: Lattice
    m: float
    g2: float

    def __post_init__(self):
        jf = lambda f: jax.jacfwd(f, holomorphic=True)
        jr = lambda f: jax.jacrev(f, holomorphic=True)
        self.actions = jax.jit(jnp.vectorize(self.action, signature='(n)->()'))
        self.daction = jax.jit(jax.grad(self.action, holomorphic=True))
        self.d2action = jax.jit(jf(jax.grad(self.action, holomorphic=True)))
        self.d3action = jax.jit(jf(jf(jax.grad(self.action, holomorphic=True))))
        self.d4action = jax.jit(jf(jf(jf(jax.grad(self.action, holomorphic=True)))))

    def invprop(self, sigma):
        """
        Returns the inverse propagator for an auxiliary field of sigma. The action
        contains a term of the form (psibar @ D @ psi).
        """
        if len(self.lattice.geom) > 0:
            raise "Cannot handle d>1"
        V = self.lattice.Nsites
        assert sigma.shape == (V,)
        D = jnp.zeros((V,V), dtype=jnp.complex64)
        # TODO use SK contour
        for site in self.lattice.sites():
            i = self.lattice.index(site)
            D = D.at[i,i].set(self.m + sigma[i])
            #D[i,i] = self.m + sigma[i]
            for neighbor in self.lattice.neighbors(site):
                j = self.lattice.index(neighbor)
                # TODO chemical potential
                if neighbor[0] < site[0]:
                    # We've crossed the temporal boundary.
                    D = D.at[i,j].set(0.5)
                else:
                    D = D.at[i,j].set(-0.5)
        return D

    def prop(self, sigma):
        """
        Returns the propagator for an auxiliary field of sigma.
        """
        M = self.invprop(sigma)
        return jnp.linalg.inv(M)

    def logdetinvprop(self, sigma):
        """
        Returns log(det(M)) + C, where M is the inverse propagator returned by
        `invprop`, and C is an arbitrary (sigma-independent) constant.
        """
        M = self.invprop(sigma)
        sgn, ld = jnp.linalg.slogdet(M)
        return ld + jnp.log(sgn)

    def action(self, sigma):
        """
        Returns the effective action (per flavor) for an auxiliary field of sigma.
        """
        return jnp.sum(sigma**2)/(2*self.g2) - self.logdetinvprop(sigma)

