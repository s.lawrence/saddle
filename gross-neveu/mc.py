#!/usr/bin/env python

"""
Gross-Neveu model, in the large-N limit. Here the expansion is computed by a Monte Carlo.
"""

import ast
from functools import partial
import jax
import jax.numpy as jnp
import numpy as np
from scipy.optimize import minimize
import sys

import gn


##################
### PARAMETERS ###

geom = ast.literal_eval(sys.argv[1])

# Number of Euclidean, Minkowski time steps
nbeta, nt = int(sys.argv[2]), int(sys.argv[3])

# Lattice parameters
m = float(sys.argv[4])
mu = float(sys.argv[5])
g2 = float(sys.argv[6])

N = int(sys.argv[7])


###############
### LATTICE ###

sk = gn.sk_contour(nbeta, nt)
lattice = gn.Lattice(geom=(),contour=sk)
model = gn.Model(lattice, m, g2)


sigma = np.zeros(lattice.Nsites)

##############
### SADDLE ###

if True:
    V = lattice.Nsites
    # Search for an arbitrary saddle point
    curvature = lambda x: jnp.linalg.norm(model.daction(x[:V]+1j*x[V:]))
    jac = jax.jit(jax.grad(curvature))
    result = minimize(curvature, np.zeros(2*V), jac=jac)
    zeta0 = result.x[:V] + 1j*result.x[V:]
    print('Minimization: ', result.success, result.message, zeta0)

# Check that first derivatives vanish.
d1S = model.daction(zeta0)
if np.linalg.norm(d1S) > 1e-5:
    print(f'!!! Norm of first derivative: {np.linalg.norm(d1S)}')
    sys.exit(1)

# Obtain the Hessian
d2S = model.d2action(zeta0)

# Diagonalize d2S
d2Svals, d2Svecs = np.linalg.eig(d2S)
inv_d2Svecs = np.linalg.inv(d2Svecs)
# d2S is now given by: d2Svecs @ np.diag(d2Svals) @ inv_d2Svecs

# Construct scaling matrix for sampling from the gaussian.
inv_sqrt_d2S = d2Svecs @ np.diag(d2Svals**-0.5) @ inv_d2Svecs


###################
### MONTE CARLO ###

NBATCH = 1 << 5
NSAMPLE = 1 << 0
rng = np.random.default_rng()

try:
    while True:
        for batch in range(NBATCH):
            pass

except KeyboardInterrupt:
    print('')
    print('== EXPECTATIONS ==')

