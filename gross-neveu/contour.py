#!/usr/bin/env python

"""
Lattice Gross-Neveu model, in the large-N limit. Here expectation values are
evaluated by a Monte Carlo. Samples are taken from the Gaussian approximation
to the saddle point.
"""

import ast
from functools import partial
import jax
import jax.numpy as jnp
import numpy as np
from scipy.optimize import minimize
import sys

import gn


##################
### PARAMETERS ###

geom = ast.literal_eval(sys.argv[1])

# Number of Euclidean, Minkowski time steps
nbeta, nt = int(sys.argv[2]), int(sys.argv[3])

# Lattice parameters
m = float(sys.argv[4])
mu = float(sys.argv[5])
g2 = float(sys.argv[6])

N = int(sys.argv[7])


###############
### LATTICE ###

sk = gn.sk_contour(nbeta, nt)
lattice = gn.Lattice(geom=(),contour=sk)
model = gn.Model(lattice, m, g2)


sigma = np.zeros(lattice.Nsites)

##############
### SADDLE ###

if True:
    V = lattice.Nsites
    # Search for an arbitrary saddle point
    curvature = lambda x: jnp.linalg.norm(model.daction(x[:V]+1j*x[V:]))
    jac = jax.jit(jax.grad(curvature))
    result = minimize(curvature, np.zeros(2*V), jac=jac)
    sigma0 = result.x[:V] + 1j*result.x[V:]
    print('Minimization: ', result.success, result.message, sigma0)

# Check that first derivatives vanish.
d1S = model.daction(sigma0)
if np.linalg.norm(d1S) > 1e-5:
    print(f'!!! Norm of first derivative: {np.linalg.norm(d1S)}')
    sys.exit(1)

# Obtain the Hessian
d2S = model.d2action(sigma0)

# Diagonalize d2S
d2Svals, d2Svecs = np.linalg.eig(d2S)
inv_d2Svecs = np.linalg.inv(d2Svecs)
# d2S is now given by: d2Svecs @ np.diag(d2Svals) @ inv_d2Svecs

# Construct scaling matrix for sampling from the gaussian.
inv_sqrt_d2S = d2Svecs @ np.diag(d2Svals**-0.5) @ inv_d2Svecs


###################
### MONTE CARLO ###

NBATCH = 1 << 5
NSAMPLE = 1 << 0
rng = np.random.default_rng()

dens = []
nums = [[] for _ in range(nt+1)]

try:
    while True:
        for batch in range(NBATCH):
            sigma = rng.normal(size=(NSAMPLE,V))
            # Sampling action
            Sp = np.sum(sigma**2,axis=-1)/2 + N*model.actions(sigma0)
            sigma = np.einsum('ab,cb->ca', inv_sqrt_d2S, sigma)

            sigma = sigma0 + sigma / np.sqrt(N)

            # True action
            S = N * model.actions(sigma)

            G = np.vectorize(model.prop,signature='(n)->(n,n)')(sigma)

            den = np.exp(-S+Sp)
            dens.append(np.mean(den))

            for t in range(nt+1):
                num = np.exp(-S+Sp) * G[:,t,0]
                nums[t].append(np.mean(num))

        print(f'{np.mean(dens)} {np.mean(dens/np.abs(dens))}')

except KeyboardInterrupt:
    print('')
    print(f'Reweighting: {np.mean(dens)}')
    print(f'Sign:        {np.mean(dens/np.abs(dens))}')
    print('== EXPECTATIONS ==')
    for t in range(nt+1):
        ex = sum(nums[t]) / sum(dens)
        print(f'{t} {ex.real} {ex.imag}')

