#!/usr/bin/env python

"""
Lattice Gross-Neveu model, in the large-N limit. Here expectation values are
evaluated by a Markov chain Monte Carlo.
"""

import ast
from functools import partial
import jax
import jax.numpy as jnp
import numpy as np
from scipy.optimize import minimize
import sys

import gn


##################
### PARAMETERS ###

geom = ast.literal_eval(sys.argv[1])

# Number of Euclidean, Minkowski time steps
nbeta, nt = int(sys.argv[2]), int(sys.argv[3])

# Lattice parameters
m = float(sys.argv[4])
mu = float(sys.argv[5])
g2 = float(sys.argv[6])

N = int(sys.argv[7])


###############
### LATTICE ###

sk = gn.sk_contour(nbeta, nt)
lattice = gn.Lattice(geom=(),contour=sk)
model = gn.Model(lattice, m, g2)


sigma = np.zeros(lattice.Nsites)

##############
### SADDLE ###

if True:
    V = lattice.Nsites
    # Search for an arbitrary saddle point
    curvature = lambda x: jnp.linalg.norm(model.daction(x[:V]+1j*x[V:]))
    jac = jax.jit(jax.grad(curvature))
    result = minimize(curvature, np.zeros(2*V), jac=jac)
    sigma0 = result.x[:V] + 1j*result.x[V:]
    print('Minimization: ', result.success, result.message, sigma0)

# Check that first derivatives vanish.
d1S = model.daction(sigma0)
if np.linalg.norm(d1S) > 1e-5:
    print(f'!!! Norm of first derivative: {np.linalg.norm(d1S)}')
    sys.exit(1)

# Obtain the Hessian
d2S = model.d2action(sigma0)

# Diagonalize d2S
d2Svals, d2Svecs = np.linalg.eig(d2S)
inv_d2Svecs = np.linalg.inv(d2Svecs)
# d2S is now given by: d2Svecs @ np.diag(d2Svals) @ inv_d2Svecs

# Construct scaling matrix for sampling from the gaussian.
inv_sqrt_d2S = d2Svecs @ np.diag(d2Svals**-0.5) @ inv_d2Svecs


###################
### MONTE CARLO ###

Ntherm = 10
Nskip = 10

try:
    # Adjust step size
    sigma = np.random.normal(size=model.lattice.Nsites)/N
    action = model.action(sigma)
    d = 1/N
    def step():
        global sigma,action
        sigmap = sigma + d*np.random.normal(size=model.lattice.Nsites)
        actionp = model.action(sigmap)
        u = np.random.uniform()
        if u < np.exp(action.real-actionp.real):
            sigma = sigmap
            action = actionp
            return 1
        return 0

    # Adjust d
    while True:
        accepted = 0
        proposed = 0
        for _ in range(100):
            accepted += step()
            proposed += 1
        if accepted > proposed*0.3 and accepted < proposed*0.4:
            break

    # Thermalize
    for _ in range(Ntherm*Nskip):
        step()

    # Sample
    while True:
        for _ in range(Nskip):
            step()
        
        Dinv = model.prop(sigma)
        cor = []
        # TODO
        print(*cor)

except KeyboardInterrupt:
    print('')
    print('== EXPECTATIONS ==')
