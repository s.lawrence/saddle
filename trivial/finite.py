#!/usr/bin/env python

"""
An N-dimensional integral, in the large-N limit.
"""

import numpy as np

##################
### PARAMETERS ###

m2 = 0.2
lamda = 0.1


###################
### MONTE CARLO ###

for N in range(1,5):
    def action_orig(phi):
        return m2/2 * np.sum(phi**2,axis=-1) + lamda/N * np.sum(phi**2,axis=-1)**2
    sd = 3
    phi = np.random.normal(size=(1000000,N)) * sd
    S = action_orig(phi)
    boltz = np.exp(-S) * np.exp(np.sum(phi**2,axis=1)/(2.*sd*sd))
    print(N, np.sum(boltz * np.sum(phi**2,axis=1)) / np.sum(boltz) / N)


#################
### TRAPEZOID ###

for N in range(1,50):
    def action(zeta):
        return N * (zeta**2 / (16*lamda) + 0.5 * np.log(m2 + 1j*zeta))
    zeta = np.linspace(-10,10,200000)
    boltz = np.exp(-action(zeta))
    print(N, (np.trapz(boltz / (m2 + 1j*zeta),zeta)/np.trapz(boltz, zeta)).real)

