#!/usr/bin/env python

"""
An N-dimensional integral, in the large-N limit.
"""

import numpy as np
from scipy.optimize import minimize

##################
### PARAMETERS ###

m2 = 0.2
lamda = 0.1


##############
### ACTION ###

def invprop(zeta):
    return m2 + 1j*zeta

def prop(zeta):
    return 1/invprop(zeta)

def logdetinvprop(zeta):
    return np.log(invprop(zeta))

@np.vectorize
def action(zeta):
    return zeta**2/(16*lamda) + 0.5 * logdetinvprop(zeta)

zeta0 = -1j * minimize(lambda z: -action(-1j*z).real, 0.1).x
M = invprop(zeta0)
Minv = prop(zeta0)

if False:
    import matplotlib.pyplot as plt
    zeta = np.linspace(-1,3,1000)
    plt.plot(zeta, action(-1j*zeta).real)
    plt.show()


# Get expectation values in two different ways.
print(1j*zeta0/(4*lamda))
print(Minv)

if True:
    # Now evaluate the expectation value, for a few different N, by Monte
    # Carlo.

    for N in range(1,20):
        def action_orig(phi):
            return m2/2 * np.sum(phi**2,axis=-1) + lamda/N * np.sum(phi**2,axis=-1)**2
        sd = 3
        phi = np.random.normal(size=(100000,N)) * sd
        S = action_orig(phi)
        boltz = np.exp(-S) * np.exp(np.sum(phi**2,axis=1)/(2.*sd*sd))
        print(N, np.sum(boltz * np.sum(phi**2,axis=1)) / np.sum(boltz) / N)

