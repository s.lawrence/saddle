#!/usr/bin/env python

"""
An N-dimensional integral, in the large-N limit. Evaluated with Monte
Carlo around the saddle point.
"""

import numpy as np
from scipy.optimize import minimize

### PARAMETERS ###

m2 = 0.2
lamda = 0.1
N = 50


##############
### ACTION ###

def invprop(zeta):
    return m2 + 1j*zeta

def prop(zeta):
    return 1/invprop(zeta)

def logdetinvprop(zeta):
    return np.log(invprop(zeta))

@np.vectorize
def action(zeta):
    return zeta**2/(16*lamda) + 0.5 * logdetinvprop(zeta)


##############
### SADDLE ###

zeta0 = -1j * minimize(lambda z: -action(-1j*z).real, 0.1).x
M = invprop(zeta0)

# Hessian
H = 1 / (8*lamda) + 0.5 * (m2 + 1j*zeta0)**-2


###################
### MONTE CARLO ###

NBATCH = 1 << 5
NSAMPLE = 1 << 10
rng = np.random.default_rng()

nums = []
dens = []

for batch in range(NBATCH):
    zeta = zeta0 + rng.normal(size=NSAMPLE) / np.sqrt(H) / np.sqrt(N)
    # Sampling action
    Sp = N * (action(zeta0) + H*(zeta-zeta0)**2/2)
    # True action
    S = N * action(zeta)
    num = np.exp(-S+Sp) / (m2 + 1j*zeta)
    den = np.exp(-S+Sp)
    nums.append(np.mean(num))
    dens.append(np.mean(den))

print(f'Expectation: {sum(nums) / sum(dens)}')
print(f'Reweighting: {np.mean(dens)}')
print(f'Sign:        {np.mean(dens/np.abs(dens))}')
