#!/usr/bin/env python

"""
An N-dimensional integral, in the large-N limit. Evaluated with Monte
Carlo around the saddle point.
"""

import numpy as np
from scipy.optimize import minimize

### PARAMETERS ###

m2 = 0.2
lamda = 0.1


##############
### ACTION ###

def invprop(zeta):
    return m2 + 1j*zeta

def prop(zeta):
    return 1/invprop(zeta)

def logdetinvprop(zeta):
    return np.log(invprop(zeta))

@np.vectorize
def action(zeta):
    return zeta**2/(16*lamda) + 0.5 * logdetinvprop(zeta)


##############
### SADDLE ###

zeta0 = -1j * minimize(lambda z: -action(-1j*z).real, 0.1).x
M = invprop(zeta0)

# Derivatives about the saddle point: A are the second derivatives, B the
# third, C the fourth.

A = 1 / (8*lamda) + 0.5 * (m2 + 1j*zeta0)**-2
B = -1j * (m2 + 1j*zeta0)**-3
C = -3 * (m2 + 1j*zeta0)**-4


###################
### MONTE CARLO ###

NBATCH = 1 << 5
NSAMPLE = 1 << 16
rng = np.random.default_rng()

Enums0 = []
Edens0 = []
Enums1 = []
Edens1 = []

for batch in range(NBATCH):
    sigma = rng.normal(size=NSAMPLE) / np.sqrt(A)

    Enum0 = (m2 + 1j*zeta0)**-1 + sigma*0
    Eden0 = 1 + sigma*0
    Enums0.append(np.sum(Enum0))
    Edens0.append(np.sum(Eden0))

    Enum1 = -sigma**2 * (m2+1j*zeta0)**-2
    Enum1 += - C/24*sigma**4 + B**2 / 6**2 * sigma**6
    Enum1 += B / 6 * 1j * sigma**4 * (m2+1j*zeta0)**-1
    Enum1 *= (m2+1j*zeta0)**-1
    Eden1 = -C / 24 * sigma**4  + B**2 / 6**2 * sigma**6
    Enums1.append(np.sum(Enum1))
    Edens1.append(np.sum(Eden1))

print(sum(Enums0)/sum(Edens0))
print(sum(Enums1)/sum(Edens0) - (sum(Enums0)/sum(Edens0))*(sum(Edens1)/sum(Edens0)))

