#!/usr/bin/env python

"""
Finding the saddle point (not attempting to evaluate expectation values) of QED
in two dimensions. Apparently, the leading saddle is not meant to be the
trivial one, so we should be able to reproduce that here.
"""

import numpy as np

Lt = 4
Lx = 4

mass = 1
coup = 1

def idx(t,x):
    return (t%Lt)*Lx + x%Lx

def dirac(A,charge):
    D = mass * np.eye(Lt*Lx) + 0j
    for t in range(Lt):
        for x in range(Lx):
            eta0 = 1
            if t == Lt-1:
                eta0 *= -1
            eta1 = (-1)**t
            D[idx(t,x),idx(t+1,x)] += -eta0/2 * np.exp(-1j * A[t,x,0])
            D[idx(t+1,x),idx(t,x)] +=  eta0/2 * np.exp( 1j * A[t,x,0])
            D[idx(t,x),idx(t,x+1)] += -eta1/2 * np.exp(-1j * A[t,x,1])
            D[idx(t,x+1),idx(t,x)] +=  eta1/2 * np.exp( 1j * A[t,x,1])
    return D

def Sgauge(A):
    S = 0.
    for t in range(Lt):
        for x in range(Lx):
            tp = (t+1)%Lt
            xp = (x+1)%Lx
            plaq = A[t,x,1] + A[t,xp,0] - A[tp,x,1] - A[t,x,0]
            S += 1.-np.cos(plaq)
    return S/coup

def Sdirac(A):
    Dp, Dm = dirac(A, 1), dirac(A, -1)
    sgnp, logdetp = np.linalg.slogdet(Dp)
    sgnm, logdetm = np.linalg.slogdet(Dm)
    if abs(sgnp-1) + abs(sgnm-1) > 1e-8:
        raise Exception(f"The dirac matrices should be positive definite! {sgnp} {sgnm}")
    return -logdetp -logdetm

def Seff(A):
    return Sgauge(A) + Sdirac(A)

A = np.zeros((Lt,Lx,2))

delta = 0.4
S = Seff(A)
for hbar in np.exp(np.linspace(5,-8,20000)):
    Ap = A + delta*np.random.normal(size=(Lt,Lx,2))
    Sp = Seff(Ap)
    print(hbar,S,Sp,Sgauge(A))
    if np.random.uniform() < np.exp((S-Sp)/hbar):
        delta *= 1.02
        A, S = Ap, Sp
    else:
        delta *= 0.98

