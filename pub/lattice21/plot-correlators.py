#!/usr/bin/env python

import matplotlib
import matplotlib.style
matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
import matplotlib.pyplot as plt
import numpy as np

with open('data/mechanics-4-20-0.2-0.1.dat') as f:
    dat = np.array([[complex(x) for x in l.split()] for l in f.readlines()])

print(dat)

plt.figure(figsize=(5,3.5), dpi=300)
plt.plot(dat[:,0], dat[:,1])
#plt.legend(loc='best')
#plt.title('Leading Order')
plt.xlabel('$T$')
plt.ylabel('$\\langle \\phi(T)\\phi(0)\\rangle$')
plt.tight_layout()
#plt.show()
plt.savefig('mechanics-lo.png', transparent=True)

plt.figure(figsize=(5,3.5), dpi=300)
plt.plot(dat[:,0], dat[:,2])
#plt.legend(loc='best')
#plt.title('NLO')
plt.xlabel('$T$')
plt.ylabel('$\\langle \\phi(T)\\phi(0)\\rangle$')
plt.tight_layout()
#plt.show()
plt.savefig('mechanics-nlo.png', transparent=True)

"""
N = finite[:,0]
expect = finite[:,1]
error = finite[:,2]

plt.figure(figsize=(5,3.5), dpi=300)
#plt.errorbar(1/N, expect, yerr=error, fmt=',', label='Monte Carlo')
plt.scatter(1/N, expect, label='Monte Carlo', color='black')
x = np.linspace(0,1,100)
plt.plot(x, 0.7-0.3*x, label='Expansion')
plt.legend(loc='best')
plt.xlabel('$1/N$')
plt.ylabel('$\\langle \\phi^2\\rangle$')
plt.tight_layout()
#plt.show()
"""
