#!/usr/bin/env python

import matplotlib
import matplotlib.pyplot as plt
matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

plt.figure(figsize=(4,3), dpi=300)
plt.tight_layout()
plt.savefig('figures/twopoint-LO.png', transparent=True)

plt.figure(figsize=(4,3), dpi=300)
plt.tight_layout()
plt.savefig('figures/twopoint-NLO.png', transparent=True)

