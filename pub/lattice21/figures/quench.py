#!/usr/bin/env python

import numpy as np

m = 1
mu = 0.5

import matplotlib
import matplotlib.pyplot as plt
matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

plt.figure(figsize=(5,3), dpi=300)

for N in [1,2,3]:
    D = 2**(2*N)

    # Pauli operators
    pauli_i = np.eye(2,dtype=np.complex128)
    pauli_x = np.array([[0,1],[1,0]],dtype=np.complex128)
    pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
    pauli_z = np.array([[1,0],[0,-1]],dtype=np.complex128)
    pauli_p = (pauli_x + 1j*pauli_y)/2
    pauli_m = (pauli_x - 1j*pauli_y)/2

    # Creation and annihilation operators (2*N)
    c = [np.eye(1) for i in range(2*N)]
    for i in range(2*N):
        for j in range(2*N):
            if i < j:
                c[i] = np.kron(pauli_x, c[i])
            elif i == j:
                c[i] = np.kron(pauli_p, c[i])
            else:
                c[i] = np.kron(pauli_i, c[i])
    cdag = [x.T.conj() for x in c]

    a,bdag = c[::2], c[1::2]
    adag,b = cdag[::2], cdag[1::2]

    H0 = np.zeros((D,D), dtype=np.complex128)
    G = np.zeros((D,D), dtype=np.complex128)
    for i in range(N):
        H0 += m * (adag[i]@a[i] + bdag[i]@b[i])
        H0 += mu * (adag[i]@a[i] - bdag[i]@b[i])
        #H0 += mu * (adag[i]@b[i] + bdag[i]@a[i])  # TODO this is a hopping term
        #G += adag[i]@a[i] + bdag[i]@b[i]
        G += adag[i]@b[i] + bdag[i]@a[i]

    H = H0 + 0.5 * G@G
    vals0, vecs0 = np.linalg.eigh(H0)
    vals, vecs = np.linalg.eigh(H)

    rho = vecs0 @ np.diag(np.exp(-vals0/1)) @ vecs0.T.conj()
    @np.vectorize
    def expect(t):
        U = vecs @ np.diag(np.exp(-1j * t * vals)) @ vecs.T.conj()
        op = H0/N
        return np.trace(rho @ U.T.conj() @ op @ U) / np.trace(rho)
    ts = np.linspace(0,5,50)
    plt.plot(ts, expect(ts).real, label=f'$N={N}$', ls='--')

plt.xlabel('$t$')
plt.ylabel('$\\langle TODO \\rangle$')
plt.legend(loc='best')
plt.tight_layout()
plt.savefig('figures/quench.png', transparent=True)

