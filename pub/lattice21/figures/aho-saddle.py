#!/usr/bin/env python

import sys
import numpy as np
from scipy.optimize import minimize

import scalar

import matplotlib
import matplotlib.pyplot as plt

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

##################
### PARAMETERS ###

# Geometry of the spatial lattice
GEOM = ()

# Number of Euclidean, Minkowski time steps
nbeta, nt = 6, 12

# Lattice parameters
m2 = 0.2
lamda = 0.1


###############
### LATTICE ###

lattice = scalar.Lattice(geom=GEOM,nbeta=nbeta,nt=nt)
model = scalar.Model(lattice, m2, lamda)
V = lattice.V


##############
### SADDLE ###

def gradient(zeta):
    """
    Returns the first derivatives of the effective action with respect to zeta.
    """
    M = model.invprop(zeta)
    Minv = np.linalg.inv(M)
    d1S = np.zeros(lattice.V,dtype=np.complex128)
    for a in range(lattice.V):
        d1S[a] += lattice.dt[a] * zeta[a] / (8*lamda)
        d1S[a] += lattice.dt[a] * 0.5j * Minv[a,a]
    return d1S

def find_saddle():
    # Search for a symmetric saddle point
    def make_zeta(x):
        # Put real and imaginary parts together.
        z = x[:lattice.nT//2] + 1j*x[lattice.nT//2:]
        z = np.concatenate([z, -1j*(1j*z).conj()])
        zeta = np.zeros(lattice.V,dtype=np.complex128)
        for t in range(lattice.nT):
            for a in lattice.slice(t):
                zeta[a] = z[t]
        return zeta
    minf = lambda x: np.linalg.norm(gradient(make_zeta(x)))
    result = minimize(minf, np.zeros(lattice.nT))
    zeta = make_zeta(result.x)
    return zeta

zeta_1 = find_saddle()
if True:
    m2 /= 2
    lamda /= 2
    zeta_2 = find_saddle()

plt.figure(figsize=(4,3), dpi=300)
plt.plot(zeta_1.imag, label='$\\hat m^2 = 0.2$, $\\hat \\lambda = 0.1$')
if True:
    plt.plot(zeta_2.imag, label='$\\hat m^2 = 0.1$, $\\hat \\lambda = 0.05$', ls='--')
plt.xlabel('$t$')
plt.ylabel('${\\rm Im}\\;\\zeta_c(t)$')
plt.ylim([-0.7,-0.1])
plt.legend(loc='best')
plt.tight_layout()
plt.savefig('figures/aho-saddle.png', transparent=True)
#plt.show()
